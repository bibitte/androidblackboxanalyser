package com.androidblackboxanalyser;

import android.net.Uri;

import com.androidblackboxanalyser.utils.BlackBoxUtils;

public class NoiseZoomableImageFragment extends ZoomableImageFragment {

    // Required empty public constructor
    public NoiseZoomableImageFragment() {
        super();
    }

    @Override
    protected Uri imageFromRecord(Record record) {
        if(record !=null){
            return BlackBoxUtils.getRecordNoiseImageURI(record,requireContext());
        }
        return null;
    }
}
