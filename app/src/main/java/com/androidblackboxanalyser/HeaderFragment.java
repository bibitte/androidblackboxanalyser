package com.androidblackboxanalyser;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.Map;

public class HeaderFragment extends Fragment {


    public static final String D_MIN = "d_min";
    private static final String PITCH_PID = "pitchPID";
    private static final String ROLL_PID = "rollPID";
    private static final String YAW_PID = "yawPID";
    public static final String FEEDFORWARD_WEIGHT = "feedforward_weight";

    protected RecordViewModel recordViewModel;
    private TableLayout tableLayout;

    public HeaderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_header, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tableLayout = requireView().findViewById(R.id.header_raw_tab);

        //retrive the model
        recordViewModel = new ViewModelProvider(requireActivity()).get(RecordViewModel.class);

        //define action on update
        recordViewModel.getSelectedRecord().observe(getViewLifecycleOwner(), record -> updateHeader());

        updateHeader();

    }


    private void updateHeader(){
        fillPID(PITCH_PID, R.id.header_tab_pitch_P, R.id.header_tab_pitch_I, R.id.header_tab_pitch_D);
        fillPID(ROLL_PID, R.id.header_tab_roll_P, R.id.header_tab_roll_I, R.id.header_tab_roll_D);
        fillPID(YAW_PID, R.id.header_tab_yaw_P, R.id.header_tab_yaw_I, R.id.header_tab_yaw_D);
        fillPID(D_MIN, R.id.header_tab_roll_D_min, R.id.header_tab_pitch_D_min, R.id.header_tab_yaw_D_min);
        fillPID(FEEDFORWARD_WEIGHT, R.id.header_tab_roll_FF, R.id.header_tab_pitch_FF, R.id.header_tab_yaw_FF);

        TypedValue out = new TypedValue();
        getResources().getValue(R.dimen.header_tab_key_layout_weight, out, true);
        float keyLayoutWeight = out.getFloat();
        getResources().getValue(R.dimen.header_tab_value_layout_weight, out, true);
        float valueLayoutWeight = out.getFloat();



        tableLayout.removeAllViews();
        TextView textView;

        if (recordViewModel.getSelectedRecord() != null && recordViewModel.getSelectedRecord().getValue() != null) {
            for (Map.Entry<String, String> entry : recordViewModel.getSelectedRecord().getValue().getHeaderMap().entrySet()) {
                // Create a new row to be added.
                TableRow tr = new TableRow(getContext());
                tr.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));

                // Create a textView to be the row-content.
                textView = new TextView(getContext());
                textView.setText(entry.getKey());
                textView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT, keyLayoutWeight));
                textView.setBackgroundResource(R.drawable.border);
                tr.addView(textView);
                textView = new TextView(getContext());
                textView.setText(entry.getValue());
                textView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT, valueLayoutWeight));
                textView.setBackgroundResource(R.drawable.border);
                tr.addView(textView);

                // Add row to TableLayout.
                tableLayout.addView(tr);
            }
        }
    }




    private void fillPID(String header, int id0, int id1, int id2) {
        String pid = null;

        if (recordViewModel.getSelectedRecord() != null && recordViewModel.getSelectedRecord().getValue() != null) {
            pid = recordViewModel.getSelectedRecord().getValue().getHeader(header);
        }

        if (pid != null) {
            String[] tab = pid.split(",");

            if (tab.length == 3) {
                updateTextView(id0, tab[0]);
                updateTextView(id1, tab[1]);
                updateTextView(id2, tab[2]);
            } else {
                updateTextView(id0, "");
                updateTextView(id1, "");
                updateTextView(id2, "");
            }
        }
    }

    private void updateTextView(int id, String str) {
        TextView textView = requireView().findViewById(id);
        if (textView != null) {
            textView.setText(str);
        }
    }
}