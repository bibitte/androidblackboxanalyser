package com.androidblackboxanalyser.utils;

public class MSPUtil {

    public static byte[] encode(byte code, byte[] data)  {
        int size;
        if (data != null) {
            size = data.length;
        } else {
            size = 0;
        }

        // new Message
        byte[] message = new byte[6 + size];
        byte checksum = 0;

        // add MessageHeap
        byte[] messageHeap = new byte[5];
        messageHeap[0] = '$';
        messageHeap[1] = 'M';
        messageHeap[2] = '<';
        messageHeap[3] = ((byte) (size & 0xFF));
        messageHeap[4] = code;

        int index = messageHeap.length;
        System.arraycopy(messageHeap, 0, message, 0, messageHeap.length);

        checksum ^= (size & 0xFF);
        checksum ^= code;


        // add Payload
        if (data != null) {
            for (byte c : data) {
                message[index++] = c;
                checksum ^= c;
            }
        }

        message[index] = checksum;

        return message;
    }

}
