package com.androidblackboxanalyser;

import android.net.Uri;

import com.androidblackboxanalyser.utils.BlackBoxUtils;

public class ResponseZoomableImageFragment extends ZoomableImageFragment{

    // Required empty public constructor
    public ResponseZoomableImageFragment() {
        super();
    }

    @Override
    protected Uri imageFromRecord(Record record) {
        if(record !=null){
            return BlackBoxUtils.getRecordResponseImageURI(record,requireContext());
        }
        return null;
    }
}
