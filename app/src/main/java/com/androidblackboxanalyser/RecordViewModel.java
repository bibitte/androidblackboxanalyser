package com.androidblackboxanalyser;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RecordViewModel extends ViewModel {

    private final MutableLiveData<Record> selectedRecord = new MutableLiveData<Record>();

    public void selectRecord(Record record) {
        selectedRecord.setValue(record);
    }
    public LiveData<Record> getSelectedRecord() {
        return selectedRecord;
    }


}
