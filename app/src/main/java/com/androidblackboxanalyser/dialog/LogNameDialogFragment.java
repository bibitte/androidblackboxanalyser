package com.androidblackboxanalyser.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.androidblackboxanalyser.R;

public class LogNameDialogFragment extends DialogFragment {



    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface LogNameDialogListener {
        void onDialogPositiveClick(LogNameDialogFragment dialog);

        void onDialogNegativeClick(LogNameDialogFragment dialog);
    }

    public void setListener(LogNameDialogListener listener) {
        this.listener = listener;
    }

    // Use this instance of the interface to deliver action events
    private LogNameDialogListener listener;
    private EditText logNameEditText;

    private String inputText;
    private String labelText;

    public LogNameDialogFragment(String labelText, String inputText) {
        this.inputText = inputText;
        this.labelText = labelText;
    }

    public LogNameDialogFragment(String labelText) {
        this(labelText,null);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View mView = inflater.inflate(R.layout.dialog_name, null);
        logNameEditText = mView.findViewById(R.id.logname);
        TextView logLabel = mView.findViewById(R.id.name_label);
        logLabel.setText(this.labelText);
        if(inputText !=null){
            logNameEditText.setText(inputText);
        }
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        AlertDialog dialog =  builder.setView(mView)
                // Add action buttons
                .setPositiveButton(R.string.ok,null)
                .setNegativeButton(R.string.cancel, null)
                .create();

        dialog.setOnShowListener(dialogInterface -> {

            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onDialogPositiveClick(LogNameDialogFragment.this);
                }
            });

            button = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
            button.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onDialogNegativeClick(LogNameDialogFragment.this);
                }
            });
        });

        return dialog;
    }

    public String getLogNameInput(){
        if(this.logNameEditText != null) {
            return this.logNameEditText.getText().toString();
        }else{
            return null;
        }
    }

    public void setError(String str){
        if(this.logNameEditText != null) {
            this.logNameEditText.setError(str);
        }
    }
}
