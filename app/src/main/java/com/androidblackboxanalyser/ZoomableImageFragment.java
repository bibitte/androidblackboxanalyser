package com.androidblackboxanalyser;

import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.github.chrisbanes.photoview.PhotoView;


public abstract class ZoomableImageFragment extends Fragment {

    protected RecordViewModel recordViewModel;

    protected PhotoView photoView;

    public ZoomableImageFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_zoomable_image, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //retrive the model
        recordViewModel = new ViewModelProvider(requireActivity()).get(RecordViewModel.class);

        photoView = requireView().findViewById(R.id.photo_view);

        //define action on update
        recordViewModel.getSelectedRecord().observe(getViewLifecycleOwner(), record -> updateImage());


        updateImage();
        photoView.setMinimumScale(1);
        photoView.setMaximumScale(5);
    }

    protected abstract Uri imageFromRecord(Record record);

    private void updateImage() {
        Matrix matrix = new Matrix();
        photoView.getSuppMatrix(matrix);
        photoView.setImageURI(imageFromRecord(recordViewModel.getSelectedRecord().getValue()));
        photoView.setSuppMatrix(matrix);
    }

}