#include <jni.h>
#include <string>
#include <asm/fcntl.h>
#include <cerrno>
#include <fcntl.h>
#include <android/log.h>
#include <cstdio>
#include <unistd.h>

extern "C"
{
#include "blackbox_decode.h"
}


static int pfd[2];
static pthread_t thr;
static const char *tag = "BLACKBOX_DECODE";



static void *thread_func(void*)
{
    ssize_t rdsz;
    char buf[128];
    while((rdsz = read(pfd[0], buf, sizeof buf - 1)) > 0) {
        if(buf[rdsz - 1] == '\n') --rdsz;
        buf[rdsz] = 0;  /* add null-terminator */
        __android_log_write(ANDROID_LOG_DEBUG, tag, buf);

    }
    return nullptr;
}

int start_logger(const char *app_name)
{
    tag = app_name;

    /* make stdout line-buffered and stderr unbuffered */
    setvbuf(stdout, 0, _IOLBF, 0);
    setvbuf(stderr, 0, _IONBF, 0);

    /* create the pipe and redirect stdout and stderr */
    pipe(pfd);
    dup2(pfd[1], 1);
    dup2(pfd[1], 2);

    /* spawn the logging thread */
    if(pthread_create(&thr, 0, thread_func, 0) == -1)
        return -1;
    pthread_detach(thr);
    return 0;
}

extern "C" void parseFile (const char *fileName){

    flightLog_t *log;
    int fd;
    int logIndex;
    fprintf(stdout, "filename : %s\n", fileName);
    platform_init();



    fd = open(fileName, O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "Failed to open log file '%s': %s\n\n", fileName, strerror(errno));
        return;
    }

    log = flightLogCreate(fd);

    if (!log) {
        fprintf(stderr, "Failed to read log file '%s'\n\n", fileName);
        return;
    }

    if (log->logCount == 0) {
        fprintf(stderr, "Couldn't find the header of a flight log in the file '%s', is this the right kind of file?\n\n", fileName);
        return;
    }

    //Decode all the logs
    for (logIndex = 0; logIndex < log->logCount; logIndex++){
        decodeFlightLog(log, fileName, logIndex);
    }

    //flightLogDestroy(log);

}

extern "C" JNIEXPORT void JNICALL
Java_com_androidblackboxanalyser_utils_BlackBoxUtils_parseLogFromJNI(
        JNIEnv* env,
        jclass clazz,jstring pathObj) {

    const char * path = env->GetStringUTFChars(pathObj, nullptr ) ;
    start_logger("antoine");
    parseFile(path);
}
