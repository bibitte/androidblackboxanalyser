package com.androidblackboxanalyser;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class PageDetailsAdapter extends FragmentStateAdapter {

    private Context context;

    // 2 - Default Constructor
    public PageDetailsAdapter(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
        this.context = fragmentActivity;

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0 :
                return new HeaderFragment();
            case 1 :
                return new NoiseZoomableImageFragment();
            case 2 :
                return new ResponseZoomableImageFragment();
            default:
                return new Fragment(); // return empty fragment
        }
    }
}



