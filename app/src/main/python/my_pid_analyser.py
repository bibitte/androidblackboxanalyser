from pid_analyser.pid_analyzer import *
import matplotlib.pyplot as plt
import gc

class MyBBLog(BB_log):
    def __init__(self, loglist, tmpdir, name):
        self.tmp_dir = tmpdir
        if not os.path.isdir(self.tmp_dir):
            os.makedirs(self.tmp_dir)
        self.name = name
        self.show='N'
        self.noise_bounds=None

        self.loglist = loglist
        self.heads = self.beheader(self.loglist)
        #dirty hack to rewirtefullpath
        for h in self.heads:
            h['tempFile'] = os.path.join(self.tmp_dir, h['tempFile'])
            print(h['tempFile'][:-3]+'01.csv')

        self.figs = self._csv_iter(self.heads)
        plt.close('all')

#use it to
def run_analysis(loglist, tmpdir, name):
    toto = MyBBLog(loglist, tmpdir, name)
    gc.collect()