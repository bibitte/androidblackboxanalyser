package com.androidblackboxanalyser.utils;

import android.content.Context;
import android.util.Log;

import com.androidblackboxanalyser.Record;
import com.google.common.collect.Iterables;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Locale;

public class CSVUtils implements UtilsContants{

    private static final String LOG_TAG = CSVUtils.class.getName();
    public static final String HEADER_TIME_US = " time (us)";

    public static long readDurationFromCSV(File f) {

        CSVRecord first = null;
        CSVRecord last = null;
        Reader in = null;
        try {
            in = new FileReader(f);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            first = Iterables.getFirst(records,null);
            last = Iterables.getLast(records,null);
            in.close();
        } catch (FileNotFoundException e) {
            Log.e(LOG_TAG,"CSV file not found",e);
            return -1;
        } catch (IOException e) {
            Log.e(LOG_TAG,"IO error",e);
            return -1;
        } finally {
            if(in!=null){
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG,"error on closing reader",e);
                }
            }
        }
        if (first != null && last !=null ) {
            try {
                String strFirst = first.get(HEADER_TIME_US);
                String strLast = last.get(HEADER_TIME_US);
                Log.d(LOG_TAG, "first : " + strFirst + " last : " + strLast);
                if (strFirst != null && strLast != null) {
                    return Long.parseLong(strLast.trim()) - Long.parseLong(strFirst.trim());
                }
            }
            catch(IllegalArgumentException e){
                Log.e(LOG_TAG,"can't find header",e);
            }

        }
        //if we are here there is error or null value => return 0
        return -1;


    }

    public static void readInfoOnCsv(Record record, Context context) {
        File tmpDir = new File(context.getApplicationContext().getFilesDir(), TMP_DIR_NAME);
        File csv = new File(tmpDir,SPLITED_BLACKBOX_LOG_NAME + String.format(Locale.FRENCH, "%02d", record.getNumber()) + BLACKBOX_CSV_EXTENTION);
        record.setDuration(readDurationFromCSV(csv));
    }
}
