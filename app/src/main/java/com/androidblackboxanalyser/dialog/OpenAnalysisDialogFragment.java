package com.androidblackboxanalyser.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.androidblackboxanalyser.DetailsActivity;
import com.androidblackboxanalyser.R;
import com.androidblackboxanalyser.Record;
import com.androidblackboxanalyser.utils.JSonUtils;

import java.io.IOException;
import java.util.ArrayList;

public class OpenAnalysisDialogFragment extends DialogFragment {

    private static final String LOG_TAG = OpenAnalysisDialogFragment.class.getName();

    String [] analysisArray = null;


    public OpenAnalysisDialogFragment(String[] analysisArray) {
        this.analysisArray = analysisArray;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_open_title)
                .setItems(analysisArray, (dialog, which) -> {
                    if(analysisArray!=null && which>=0 && which <analysisArray.length ) {
                        ArrayList<Record> recordList = null;
                        try {
                            recordList = JSonUtils.readJson(analysisArray[which], getActivity());
                        } catch (IOException e) {
                            Log.e(LOG_TAG,"I/O error during opening analysis",e);
                        }
                        if (recordList!=null && !recordList.isEmpty()) {
                            Intent intent = new Intent(getActivity(), DetailsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putParcelableArrayListExtra(DetailsActivity.RECORD_LIST_KEY, recordList);
                            intent.putExtra(DetailsActivity.RECORD_KEY, recordList.get(0));
                            startActivity(intent);
                        }
                    }
                });
        return builder.create();
    }

}
