package com.androidblackboxanalyser.utils;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.util.Log;

public class USBUtil {

    public static final int TIMEOUT = 1000;

    public static int MSC_PROTOCOL = 80;

    private static final String LOG_TAG = USBUtil.class.getName();

    private static final byte REBOOT_MSC_CODE = ((byte) (68 & 0xFF));
    private static final byte[] REBOOT_MSC_DATA = {((byte) (2 & 0xFF))};

    public static boolean isMassStorage(UsbDeviceConnection connection, UsbDevice device){
        if(device!=null && connection!=null) {
            for (int i = 0; i < device.getInterfaceCount(); i++) {
                if (device.getInterface(i).getInterfaceProtocol() == MSC_PROTOCOL) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean activateMassStorage(UsbDeviceConnection connection, UsbDevice device){
        if(device!=null && connection!=null) {
            UsbEndpoint epOut = null, epIn = null;
            UsbInterface usbInterface;


            for (int i = 0; i < device.getInterfaceCount(); i++) {
                usbInterface = device.getInterface(i);

                Log.d(LOG_TAG, "--------------------------------------------");
                Log.d(LOG_TAG, "interface name : " + usbInterface.getName());
                Log.d(LOG_TAG, "interface id : " + usbInterface.getId());
                Log.d(LOG_TAG, "interface protocol  : " + usbInterface.getInterfaceProtocol());


                Log.d(LOG_TAG, "interface endpointCount  : " + usbInterface.getEndpointCount());


                connection.claimInterface(usbInterface, true);

                for (int j = 0; j < usbInterface.getEndpointCount(); j++) {
                    UsbEndpoint ep = usbInterface.getEndpoint(j);
                    Log.d(LOG_TAG, "----------------");
                    Log.d(LOG_TAG, "endPoint adress : " + ep.getAddress());
                    Log.d(LOG_TAG, "endPoint direction : " + ep.getDirection());
                    Log.d(LOG_TAG, "endPoint type : " + ep.getType());
                    if (ep.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                        if (ep.getDirection() == UsbConstants.USB_DIR_OUT) {
                            // from host to device
                            epOut = ep;

                        } else if (ep.getDirection() == UsbConstants.USB_DIR_IN) {
                            // from device to host
                            epIn = ep;
                        }
                    }
                }
            }

            if (epOut != null) {
                byte[] message = MSPUtil.encode(REBOOT_MSC_CODE, REBOOT_MSC_DATA);
                connection.bulkTransfer(epOut, message, message.length, TIMEOUT);
                return true;

            }else{
                Log.d(LOG_TAG, "no Out end point ");
                return false;
            }
        }
        return false;
    }

}
